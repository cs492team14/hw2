CFLAGS += -pthread -std=c99 -Wall -Wextra -g

name=VMsimulator

$(name): main.c memory.c pageTable.c
	$(CC) $(CFLAGS) -o $@ $^ -lm

clean:
	rm -f $(name)
