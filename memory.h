//
// Created by austin on 4/7/17.
//

#ifndef VMSIMULATOR_MEMORY_H
#define VMSIMULATOR_MEMORY_H

#include "pageTable.h"

#define MEMORY_SIZE 512

typedef enum {
    FIFO,
    LRU,
    CLOCK
} Algorithm;

long PAGE_SWAPS;

/**
 * Init PAGES_IN_MEMORY array `MEMORY_SIZE / page size` long
 * Split up array ownership equally over programs into `n` pieces with `m` slots each
 * Load first `m` pages each program
 *
 * @param ptables
 * @param totalPrograms
 * @param pageSize
 */
void memory_init(PTable *ptables, int totalPrograms, int pageSize, int prepaging, Algorithm algorithm);

/**
 * - Tries to load `location` from program's memory slot
		- Go to `program name`'s segment of the total memory
		- Get program's page table
		- Get memory location's page number from the page table
		- Check if that is loaded in the program's main memory segment
	- If not there -> page fault
		- Decide which memory slot to get replace
		- Here is where we use the ALGORITHMS
 *
 * @param program
 * @param location
 */
void locate(int program, int location);

#endif //VMSIMULATOR_MEMORY_H
