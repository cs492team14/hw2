//
// Created by austin on 4/7/17.
//

#ifndef VMSIMULATOR_PAGETABLES_H
#define VMSIMULATOR_PAGETABLES_H

#define NOT_LOADED 0
#define LOADED 1

#define NOT_REFERENCED 0
#define REFERENCED 1

unsigned long PTABLE_TIMER;

typedef struct ptable* PTable;
struct ptable {
    int* pageNums;
    int* validBits;
    int* referenceBits;
    unsigned long* lastAccess;
    unsigned long* timeLoaded;
    int numPages;
    int numPagesLoaded;
    int pageSize;
    int program;
    int pageIndexStart;
    int clockHandIndex;
};

/**
 *
 * @param numPages
 * @param pageSize
 * @param programName
 * @param pageIndexStart
 * @return table
 */
PTable makePTable(int numPages, int pageSize, int programName, int pageIndexStart);

/**
 * Memory freeing
 * @param table
 */
void freePTable(PTable table);

/**
 * Translates from `memory location` to index in page numbers array
 * Accounts for `page index start` used for unique naming
 * @param table
 * @param memoryLocation
 * @return
 */
int ptable_getPageNumber(PTable table, int memoryLocation);

/**
 * Gets the page index from the unique page number
 * @param table
 * @param pageNumber
 * @return
 */
int ptable_getPageIndex(PTable table, int pageNumber);

/**
 *
 * @param table
 * @param pageIndex
 * @return
 */
int ptable_getPageNumberByIndex(PTable table, int pageIndex);

/**
 * 'Looks up' page in the `page numbers` array
 * Updates the last access time
 * @param table
 * @param pageNumber
 */
void ptable_lookup(PTable table, int pageNumber);

/**
 * Set Valid bits for page
 * @param table
 * @param pageIndex
 */
void ptable_loadPageByIndex(PTable table, int pageIndex);

/**
 * Opposite of load
 * Doesn't increment last access or timer
 * @param table
 * @param pageIndex
 */
void ptable_unloadPageByIndex(PTable table, int pageIndex);

/**
 * Checks if a page at pageIndex exists
 * @param table
 * @param pageIndex
 */
int ptable_hasPageByIndex(PTable table, int pageIndex);

#endif //VMSIMULATOR_PAGETABLES_H
