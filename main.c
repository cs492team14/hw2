#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include "pageTable.h"
#include "memory.h"

#ifndef FALSE

#define FALSE 0
#define TRUE 1

#endif

void printUsage() {
    printf("USAGE: VMsimulator [path to plist file] [path to ptrace file] [size of page] [algorithm] [pre-paging flag]\n");
}

int main(int argc, char *argv[]) {
    // P1: plist file
    // P2: ptrace file
    // P3: Page size
    // P4: Algorithm: { FIFO, LRU, CLOCK }
    // P5: Pre-paging flag: { +, - }
    // USAGE: VMsimulator plist ptrace 2 FIFO +

    /*ensures correct number of arguments*/
    if (argc != 6) {
        printUsage();
        perror("Usage error: needs 5 arguments.\n");
        exit(1);
    }

    int pageSize = atoi(argv[3]);

    char *algorithmStr = argv[4];
    Algorithm algorithm;
    if (strcmp(algorithmStr, "FIFO") == 0) {
        algorithm = FIFO;
    } else if (strcmp(algorithmStr, "LRU") == 0) {
        algorithm = LRU;
    } else if (strcmp(algorithmStr, "CLOCK") == 0) {
        algorithm = CLOCK;
    } else {
        printUsage();
        printf("Usage error: Algorithm must be one of { FIFO, LRU, CLOCK }, not %s.\n", algorithmStr);
        exit(1);
    }

    char *prepageStr = argv[5];
    int prepage;
    if (strcmp(prepageStr, "+") == 0) {
        prepage = TRUE;
    } else if (strcmp(prepageStr, "-") == 0) {
        prepage = FALSE;
    } else {
        printUsage();
        printf("Usage error: Prepaging flag must be either '-' or '+', not %s.\n", prepageStr);
        exit(1);
    }

    /*reading from the files*/
    FILE *plist;
    int plistNum, plistMem, numLines = 0;
    double pages, mem;
    int startIndex = 0;

    /*opening plist*/
    plist = fopen(argv[1], "r");
    if (plist == NULL) {
        fprintf(stderr, "Can't open input file\n");
        exit(1);
    }
    /*count the number of lines/programs*/
    char c;
    while (!feof(plist)) {
        c = fgetc(plist);
        if (c == '\n') {
            numLines++;
        }
    }
    /*initialize the page tables array*/
    int numTables = numLines - 1;
    PTable pTables[numTables];
//    printf("lines: %i\n", numLines - 1);
    /*reset to the top of the file*/
    fseek(plist, 0, SEEK_SET);

    /*while we aren't at the end of the file*/
    while (!feof(plist)) {
        fscanf(plist, "%i %i\n", &plistNum, &plistMem);
        /*determine # of pages needed*/
        mem = plistMem;
        // zero indexed
        // Take the ceiling as pages will overflow
        pages = ceil((mem + 1) / (double) pageSize);
//      printf("%i\n", startIndex);
        /*make PTable for each program*/
        pTables[plistNum] = makePTable((int) pages, pageSize, plistNum, startIndex);
        startIndex += pages;
    }

    fclose(plist);

    // Now initialize the memory
    memory_init(pTables, numTables, pageSize, prepage, algorithm);

    /*Reading the requests from a file*/
    FILE *ptrace;
    int programNum, location;

    ptrace = fopen(argv[2], "r");
    if (ptrace == NULL) {
        fprintf(stderr, "Can't open input file\n");
        exit(1);
    }

    while (!feof(ptrace)) {
        fscanf(ptrace, "%i %i\n", &programNum, &location);
//      printf("location: %i\n", location);
        locate(programNum, location);
    }
    fclose(ptrace);

    printf("\nInfo: \n");
    printf("Number of programs: %i\n", numLines - 1);
    printf("Page size: %i\n", pageSize);
    printf("Algorithm: %s\n", algorithmStr);
    printf("Pre-paging: ");
    if (prepage) {
        printf("Yes\n");
    } else {
        printf("No\n");
    }
    printf("Number of page swaps: %li\n", PAGE_SWAPS);
    printf("Ending time: %li\n", PTABLE_TIMER);

    // Free the memory
    for (int i = 0; i < numTables; i++) {
        freePTable(pTables[i]);
    }

    return 0;
}
