# Homework 2

## Structures & Files

For each programs in plist:  
- Make a page table with `program total memory address` / page size page locations  


### `pageTable.h` + `pageTable.c`  
Page Table:  
- page numbers
- valid bits
- last access time
- total number of pages
- size of each page
- program number
- page index start

Methods:
- `PTable makePTable(int numPages, int pageSize, int programNumber, int pageIndexStart)`
	- Creates a Page Table from the info
	- Uses `pageIndexStart` to uniquely name pages
	
- `int ptable_getPageNumber(PTable table, int memoryLocation)`
	- Translates from `memory location` to index in page numbers array
	- Accounts for `page index start` used for unique naming 

- `void ptable_lookup(PTable table, int pageNumber)`
	- 'Looks up' page in the `page numbers` array
		- Updates the last access time

### `memory.h` + `memory.c`
main memory:
- page tables for each program
- array of 512 page `page name` slots
- array of `{progam name, start index, end index}` to represent memory 'ownership'
	- The `program name` is index in `page table` array

Methods:  
- `init(progam page tables, total programs, total size, page size)`
	- Makes an array of 'pages' `total size / page size` long
	- Split up array ownership equally over programs into `n` pieces with `m` slots each
	- Load first `m` pages each program 

- `locate(program name, location to locate)`
	- Tries to locate `location to locate` from program's memory slot
		- Go to `program name`'s segment of the total memory
		- Get program's page table 
		- Get memory location's page number from the page table
		- Check if that is loaded in the program's main memory segment
	- If not there -> page fault
		- Decide which memory slot to get replace
		- Here is where we use the ALGORITHMS

Handles loading and replacing for each process.  
Given a page to load and the process that is trying to locate it.  
Deal with the replacement algorithms.