//
// Created by austin on 4/7/17.
//

#include <stdlib.h>
#include "pageTable.h"

#define INITIAL_CLOCK -1

// GLOBAL TIMING COUNTER
unsigned long PTABLE_TIMER = 0;

/**
 *
 * @param numPages
 * @param pageSize
 * @param programName
 * @param pageIndexStart
 * @return table
 */
PTable makePTable(int numPages, int pageSize, int programName, int pageIndexStart) {
    PTable table = (PTable) malloc(sizeof(struct ptable));
    table->numPages = numPages;
    table->pageSize = pageSize;
    table->pageIndexStart = pageIndexStart;
    table->program = programName;
    table->numPagesLoaded = 0; // None in memory at the start
    table->clockHandIndex = INITIAL_CLOCK; // Starts pointing at NOTHING
    // The 'page' arrays
    table->lastAccess = (unsigned long*) malloc(numPages * sizeof(unsigned long));
    table->timeLoaded = (unsigned long*) malloc(numPages * sizeof(unsigned long));
    table->validBits = (int*) malloc(numPages * sizeof(int));
    table->referenceBits = (int*) malloc(numPages * sizeof(int));
    table->pageNums = (int*) malloc(numPages * sizeof(int));

    // Initialize all array
    for (int i = 0; i < numPages; i++) {
        table->pageNums[i] = i + pageIndexStart;
        table->validBits[i] = NOT_LOADED; // Not in the memory to start
        table->referenceBits[i] = NOT_REFERENCED; // Not referenced to start
        table->lastAccess[i] = 0; // Not accessed yet
        table->timeLoaded[i] = 0; // Not loaded yet
    }

    return table;
}

void freePTable(PTable table) {
    table->numPages = 0;
    table->pageSize = 0;
    table->pageIndexStart = 0;
    table->program = 0;
    table->numPagesLoaded = 0;
    table->clockHandIndex = 0;
    // The 'page' arrays
//    free(table->lastAccess);
//    free(table->timeLoaded);
//    free(table->validBits);
//    free(table->referenceBits);
//    free(table->pageNums);

    free(table);
}

/**
 * Gets page's unique number
 * Translates from `memory location` to index in page numbers array
 * Accounts for `page index start` used for unique naming
 * @param table
 * @param memoryLocation
 * @return
 */
int ptable_getPageNumber(PTable table, int memoryLocation) {
    return table->pageIndexStart + (memoryLocation / table->pageSize);
}

/**
 * Offset by the pageIndexStart
 * @param table
 * @param pageIndex
 * @return
 */
int ptable_getPageNumberByIndex(PTable table, int pageIndex) {
    return table->pageIndexStart + pageIndex;
}

int ptable_getPageIndex(PTable table, int pageNumber) {
    return pageNumber - table->pageIndexStart;
}

/**
 * Looks up by index instead of by unique page number
 * @param table
 * @param pageIndex
 */
void ptable_lookupByIndex(PTable table, int pageIndex) {
    table->lastAccess[pageIndex] = PTABLE_TIMER;
    // Mark as referenced
    table->referenceBits[pageIndex] = REFERENCED;
    PTABLE_TIMER++; // Count up the global timer
}

/**
 * 'Looks up' page in the `page numbers` array
 * Updates the last access time
 * @param table
 * @param pageNumber
 */
void ptable_lookup(PTable table, int pageNumber) {
    return ptable_lookupByIndex(table, pageNumber - table->pageIndexStart);
}

/**
 * Set Valid bit and such
 * @param table
 * @param pageIndex
 */
void ptable_loadPageByIndex(PTable table, int pageIndex) {
    // If the Clock hand isn't pointing to anything, direct it here
    if (table->clockHandIndex == INITIAL_CLOCK) {
        table->clockHandIndex = pageIndex;
    }

    table->timeLoaded[pageIndex] = PTABLE_TIMER;
    table->lastAccess[pageIndex] = PTABLE_TIMER;
    table->validBits[pageIndex] = LOADED;
    PTABLE_TIMER++; // Count up!
    table->numPagesLoaded++;
}

/**
 * Opposite of load
 * Doesn't increment last access or timer
 * @param table
 * @param pageIndex
 */
void ptable_unloadPageByIndex(PTable table, int pageIndex) {
    table->validBits[pageIndex] = NOT_LOADED;
    table->numPagesLoaded--;
}

/**
 *
 * @param table
 * @param pageIndex
 */
int ptable_hasPageByIndex(PTable table, int pageIndex) {
    if (pageIndex < table->numPages) {
        return 1; // Has it
    }
    return 0; // Doesn't
}
