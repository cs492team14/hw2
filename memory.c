//
// Created by austin on 4/7/17.
//

#include <stdlib.h>
#include <stdio.h>
#include <limits.h>
#include "memory.h"

// GLOBALS
PTable *PROGRAM_PTABLES;
int PAGES_PER_PROGRAM;
int NUM_PAGES;
int NUM_PROGRAMS;
int PREPAGING;
Algorithm ALGORITHM;

/**
 * Init PAGES_IN_MEMORY array `MEMORY_SIZE / page size` long
 * Split up array ownership equally over programs into `n` pieces with `m` slots each
 * Load first `m` pages each program
 *
 * @param ptables
 * @param totalPrograms
 * @param pageSize
 */
void memory_init(PTable *ptables, int totalPrograms, int pageSize, int prepaging, Algorithm algorithm) {
    PAGE_SWAPS = 0;
    NUM_PAGES = MEMORY_SIZE / pageSize;
    NUM_PROGRAMS = totalPrograms;
    PROGRAM_PTABLES = ptables;
    ALGORITHM = algorithm;
    PREPAGING = prepaging;
    // Associate PAGES_IN_MEMORY array with total programs
    // All programs get equal space, even if there is overflow
    PAGES_PER_PROGRAM = NUM_PAGES / NUM_PROGRAMS;

    // Load first 'm' pages of each program into the 'WORKING SET'
    for (int program = 0; program < NUM_PROGRAMS; program++) {
        for (int m = 0; m < PAGES_PER_PROGRAM; m++) {
            // only locate page if exists
            if (ptable_hasPageByIndex(PROGRAM_PTABLES[program], m)) {
                ptable_loadPageByIndex(PROGRAM_PTABLES[program], m);
            } else {
                // Otherwise end loading early
                break;
            }
        }
    }
}

/**
 * Least Recently Used
 * @param program
 * @param pageNumber
 */
void pagefaultLRU(int program, int pageIndex) {
    // Cycle through all the pages of the program's page table
    // Pick the oldest accessed (lowest number) and remove it
    PTable progPTable = PROGRAM_PTABLES[program];
    // Start with the first indexed page
    int oldestPageIndex = 0;
    unsigned long oldestTime = ULONG_MAX;
    // Linear search
    for (int i = 0; i < progPTable->numPages; i++) {
        // Make sure it's loaded
        if (progPTable->validBits[i]
            && progPTable->lastAccess[i] < oldestTime) {
            oldestTime = progPTable->lastAccess[i];
            oldestPageIndex = i;
        }
    }

    // Unload page at index `oldestPageIndex`
    ptable_unloadPageByIndex(progPTable, oldestPageIndex);

    // Load page with `pageNumber`
    ptable_loadPageByIndex(progPTable, pageIndex);
}

void pagefaultFIFO(int program, int pageIndex) {
    // Cycle through all the pages of the program's page table
    // Pick the oldest loaded and remove it
    PTable progPTable = PROGRAM_PTABLES[program];
    // Start with the first indexed page
    int oldestPageIndex = 0;
    unsigned long oldestTime = ULONG_MAX;

    // Linear search
    for (int i = 0; i < progPTable->numPages; i++) {
        // Make sure the page is loaded
        if (progPTable->validBits[i] &&
            progPTable->timeLoaded[i] < oldestTime) {
            oldestTime = progPTable->timeLoaded[i];
            oldestPageIndex = i;
        }
    }

    // Unload page at index `oldestPageIndex`
    ptable_unloadPageByIndex(progPTable, oldestPageIndex);

    // Load page
    ptable_loadPageByIndex(progPTable, pageIndex);
}

void pagefaultCLOCK(int program, int pageIndex) {
    PTable progPTable = PROGRAM_PTABLES[program];

    int pageIndexToEvict = -1;
    // Check the clock hand in the program's PTable
    while (pageIndexToEvict == -1) {
        if (!progPTable->referenceBits[progPTable->clockHandIndex]) {
            // If it hasn't been referenced, evict it!
            pageIndexToEvict = progPTable->clockHandIndex;
        } else {
            // Otherwise clear the R bit and advance hand to the next page in the working set
            progPTable->referenceBits[progPTable->clockHandIndex] = NOT_REFERENCED;
        }
        // Still tick the hand onwards

        // Advance
        int nextHandPos = progPTable->clockHandIndex;
        while (1) {
            if (nextHandPos + 1 >= progPTable->numPages) {
                // cycle back around
                nextHandPos = 0;
            } else {
                nextHandPos++;
            }

            if (nextHandPos == progPTable->clockHandIndex) {
                // In an infinite loop where no pages are in the working set, escape!
                break;
            }

            // Check if it's in the working set
            if (progPTable->validBits[nextHandPos]) {
                // Found it
                progPTable->clockHandIndex = nextHandPos;
                break;
            }
        }
    }

    // Evict old page
    ptable_unloadPageByIndex(progPTable, pageIndexToEvict);

    // Load page
    ptable_loadPageByIndex(progPTable, pageIndex);
}


/**
 * Handle Page Faults and use appropriate algorithms
 * @param program
 * @param pageNumber
 */
void pagefault(int program, int pageIndex) {
    // Todo: should these be counted somewhere else?
    PAGE_SWAPS++; // Count a page swap

    PTable progPTable = PROGRAM_PTABLES[program];

    // Deal with the first page loading
    if (progPTable->numPagesLoaded < PAGES_PER_PROGRAM) {
        // Just load it if there is room
        ptable_loadPageByIndex(progPTable, pageIndex);
    } else {
        // Otherwise have to evict
        switch (ALGORITHM) {
            case LRU:
                pagefaultLRU(program, pageIndex);
                break;
            case FIFO:
                pagefaultFIFO(program, pageIndex);
                break;
            case CLOCK:
                pagefaultCLOCK(program, pageIndex);
                break;
        }
    }

    if (PREPAGING) {
        int nextPageIndex = pageIndex + 1;
        /* Break condition is that we check every page and they're allll loaded
        or we find the next unloaded page */
        while (nextPageIndex != pageIndex && progPTable->validBits[nextPageIndex]) {
            // Get the next contiguous pageNumber that is not loaded
            if (nextPageIndex + 1 >= progPTable->numPages) {
                // cycle back around
                nextPageIndex = 0;
            } else {
                nextPageIndex++;
            }
        }

        // Load the next contiguous if we found it
        if (nextPageIndex != pageIndex) {
            // If there is enough space in the program's allocated memory
            if (progPTable->numPagesLoaded < PAGES_PER_PROGRAM) {
                // Just load it
                ptable_loadPageByIndex(progPTable, nextPageIndex);
            } else {
                // Otherwise have to evict something
                switch (ALGORITHM) {
                    case LRU:
                        pagefaultLRU(program, nextPageIndex);
                        break;
                    case FIFO:
                        pagefaultFIFO(program, nextPageIndex);
                        break;
                    case CLOCK:
                        pagefaultCLOCK(program, nextPageIndex);
                        break;
                }
            }
        }
    }


}

/**
 * - Tries to load `location` from program's memory slot
		- Go to `program name`'s segment of the total memory
		- Get program's page table
		- Get memory location's page number from the page table
		- Check if that is loaded in the program's main memory segment
	- If not there -> page fault
		- Decide which memory slot to get replace
		- Here is where we use the ALGORITHMS
 *
 * @param program
 * @param location
 */
void locate(int program, int location) {
    PTable currentTable = PROGRAM_PTABLES[program];
    /*page number within the current table*/
    int currentPageNum = ptable_getPageNumber(currentTable, location);
    int currentPageIndex = ptable_getPageIndex(currentTable, currentPageNum);

    /*Page is loaded into memory*/
    if (currentTable->validBits[currentPageIndex]) {
        ptable_lookup(currentTable, currentPageNum);
    }
        /*page fault*/
    else {
        pagefault(program, currentPageIndex);
    }

}
